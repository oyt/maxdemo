//c1
var c1 = echarts.init(document.getElementById('c1'));
var widthc1 = $('#c1').width();
var heightc1 = $('#c1').height();
$('#c1')
    .css('width', widthc1)
    .css('height', heightc1);
var c1a = {
    // 鼠标放置提示框
    tooltip: {},
    xAxis: {
        type: 'category',
        data: ['8.1', '8.2', '8.3', '8.4', '8.5', '8.6', '8.7'],
        // X轴样式
        axisLine: {
            lineStyle: {
                color: '#eee',
                width: 1, //这里是为了突出显示加上的
            }
        },
        //X轴刻度平均分布
        axisLabel: {
            fontSize: '30em',
            interval: 0
        }
    },
    yAxis: {
        type: 'value',
        axisTick: {
            show: false
        },
        //刻度线间距
        max: 300,
        min: 0,
        splitNumber: 3,
        axisLabel: {
            fontSize: '30em'
        },
        //XY轴样式
        axisLine: {
            show: false,
            lineStyle: {
                color: '#ccc',
                width: 1, //这里是为了突出显示加上的

            },

        },
        //网格线
        splitLine: {
            show: true,
            lineStyle: {
                color: '#0ea578',
                width: 0.5,
                type: 'dashed',
            }
        },

    },
    //偏移值
    grid: {
        left: '10%',
        top: '20%',
        bottom: '30%',
        containLabel: true
    },
    series: [
        {
            type: 'line',
            //曲线弯度
            smooth: 0.7,
            areaStyle: {
                normal: {}
            },
            data: [129, 210, 98, 229, 218, 96, 184],
            itemStyle: {
                normal: {
                    //折线区域颜色渐变
                    color: new echarts.graphic.LinearGradient(0, 0, 0, 1[
                        {
                            offset: 0,
                            color: '#1C494B'
                        },
                            {
                                offset: 0.5,
                                color: 'rgba(255,255,255,0.5)'
                            },
                            {
                                offset: 1,
                                color: 'transparent'
                            }
                        ]),
                    color: '#074F4B', //改变折线点的颜色
                    borderColor: '#075652',
                    lineStyle: {
                        color: '#0ea377', //改变折线颜色
                        width: 1
                    }
                }
            },
            label: {
                normal: {
                    show: true,
                    position: 'top',
                    color: '#fff',
                    fontSize: 9,
                }
            }
        }
    ]
};
c1.setOption(c1a);
//屏幕自适应
window.addEventListener("resize", function () {
    c1.resize();
})

//c2
var c2 = echarts.init(document.getElementById('c2'));
var widthc2 = $('#c2').width();
var heightc2 = $('#c2').height();
$('#c2')
    .css('width', widthc2)
    .css('height', heightc2);
var c2a = {
    tooltip: {},
    xAxis: {
        type: 'category',
        data: ['8.1', '8.2', '8.3', '8.4', '8.5', '8.6', '8.7'],
        axisLine: {
            lineStyle: {
                color: '#eee',
                width: 1, //这里是为了突出显示加上的
            }
        },
        //X轴刻度平均分布
        axisLabel: {
            fontSize: '30em',
            interval: 0
        }
    },
    yAxis: {
        type: 'value',
        axisTick: {
            show: false
        },
        axisLabel: {
            fontSize: '30em'
        },
        //刻度线间距
        max: 300,
        min: 0,
        splitNumber: 3,
        //XY轴样式
        axisLine: {
            show: false,
            lineStyle: {
                color: '#ccc',
                width: 1, //这里是为了突出显示加上的
            }
        },
        //网格线
        splitLine: {
            show: true,
            lineStyle: {
                color: '#0ea578',
                width: 0.5,
                type: 'dashed',
            }
        },
    },
    //偏移值
    grid: {
        left: '10%',
        top: '20%',
        bottom: '30%',
        containLabel: true
    },
    series: [
        {
            type: 'line',
            //曲线弯度
            smooth: 0.7,
            areaStyle: {normal: {}},
            data: [129, 210, 98, 229, 218, 96, 184],
            itemStyle: {
                normal: {
                    //折线区域颜色渐变
                    color: new echarts.graphic.LinearGradient(0, 0, 0, 1[
                        {
                            offset: 0,
                            color: '#1C494B'
                        },
                            {
                                offset: 0.5,
                                color: 'rgba(255,255,255,0.5)'
                            },
                            {
                                offset: 1,
                                color: 'transparent'
                            }
                        ]),
                    color: '#074F4B', //改变折线点的颜色
                    borderColor: '#075652',
                    lineStyle: {
                        color: '#0ea377', //改变折线颜色
                        width: 1
                    }
                }
            },
            label: {
                normal: {
                    show: true,
                    position: 'top',
                    fontSize: 9,
                    color: '#fff',
                }
            }
        }
    ]
};
c2.setOption(c2a);
//屏幕自适应
window.addEventListener("resize", function () {
    c2.resize();
})

//c3
var c3 = echarts.init(document.getElementById('c3'));
var widthc3 = $('#c3').width();
var heightc3 = $('#c3').height();
$('#c3')
    .css('width', widthc3)
    .css('height', heightc3);
var c3a = {
    tooltip: {},
    xAxis: {
        type: 'category',
        data: ['8.1', '8.2', '8.3', '8.4', '8.5', '8.6', '8.7'],
        axisLine: {
            lineStyle: {
                color: '#eee',
                width: 1, //这里是为了突出显示加上的
            }
        },
        //X轴刻度平均分布
        axisLabel: {
            fontSize: '30em',
            interval: 0
        }
    },
    yAxis: {
        type: 'value',
        axisTick: {
            show: false
        },
        axisLabel: {
            fontSize: '30em'
        },
        //刻度线间距
        max: 300,
        min: 0,
        splitNumber: 3,
        //XY轴样式
        axisLine: {
            show: false,
            lineStyle: {
                color: '#ccc',
                width: 1, //这里是为了突出显示加上的

            }
        },
        //网格线
        splitLine: {
            show: true,
            lineStyle: {
                color: '#0ea578',
                width: 0.5,
                type: 'dashed',
            }
        },

    },
    //偏移值
    grid: {
        left: '5%',
        top: '20%',
        bottom: '23%',
        containLabel: true
    },
    series: [
        {
            type: 'line',
            //曲线弯度
            smooth: 0.7,
            areaStyle: {normal: {}},
            data: [129, 210, 98, 229, 218, 96, 184],
            itemStyle: {
                normal: {
                    //折线区域颜色渐变
                    color: new echarts.graphic.LinearGradient(0, 0, 0, 1[
                        {
                            offset: 0,
                            color: '#1C494B'
                        },
                            {
                                offset: 0.5,
                                color: 'rgba(255,255,255,0.5)'
                            },
                            {
                                offset: 1,
                                color: 'transparent'
                            }
                        ]),
                    color: '#074F4B', //改变折线点的颜色
                    borderColor: '#075652',
                    lineStyle: {
                        color: '#0ea377', //改变折线颜色
                        width: 1
                    }
                }
            },
            label: {
                normal: {
                    show: true,
                    position: 'top',
                    fontSize: 9,
                    color: '#fff',
                }
            }
        }
    ]
};
c3.setOption(c3a);
//屏幕自适应
window.addEventListener("resize", function () {
    c3.resize();
})

//c4
var c4 = echarts.init(document.getElementById('c4'));
var widthc4 = $('#c4').width();
var heightc4 = $('#c4').height();
$('#c4')
    .css('width', widthc4)
    .css('height', heightc4);
var c4a = {
    tooltip: {},
    xAxis: {
        type: 'category',
        data: ['8.1', '8.2', '8.3', '8.4', '8.5', '8.6', '8.7'],
        axisLine: {
            lineStyle: {
                color: '#eee',
                width: 1, //这里是为了突出显示加上的
            }
        },
        //X轴刻度平均分布
        axisLabel: {
            interval: 0,
            fontSize: '30em',
        }
    },
    yAxis: {
        type: 'value',
        axisTick: {
            show: false
        },
        axisLabel: {
            fontSize: '30em'
        },
        //刻度线间距
        max: 300,
        min: 0,
        splitNumber: 3,
        //XY轴样式
        axisLine: {
            show: false,
            lineStyle: {
                color: '#ccc',
                width: 1, //这里是为了突出显示加上的

            }
        },
        //网格线
        splitLine: {
            show: true,
            lineStyle: {
                color: '#0ea578',
                width: 0.5,
                type: 'dashed',
            }
        },

    },
    //偏移值
    grid: {
        left: '5%',
        top: '20%',
        bottom: '23%',
        containLabel: true
    },
    series: [
        {
            type: 'line',
            //曲线弯度
            smooth: 0.7,

            areaStyle: {normal: {}},
            data: [129, 210, 98, 229, 218, 96, 184],
            itemStyle: {
                normal: {
                    //折线区域颜色渐变
                    color: new echarts.graphic.LinearGradient(0, 0, 0, 1[
                        {
                            offset: 0,
                            color: '#1C494B'
                        },
                            {
                                offset: 0.5,
                                color: 'rgba(255,255,255,0.5)'
                            },
                            {
                                offset: 1,
                                color: 'transparent'
                            }
                        ]),
                    color: '#074F4B', //改变折线点的颜色
                    borderColor: '#075652',
                    lineStyle: {
                        color: '#0ea377', //改变折线颜色
                        width: 1
                    }
                }
            },
            label: {
                normal: {
                    show: true,
                    position: 'top',
                    fontSize: 9,
                    color: '#fff',
                }
            }
        }
    ]
};
c4.setOption(c4a);
//屏幕自适应
window.addEventListener("resize", function () {
    c4.resize();
})

//柱状图
var widthz1 = $('#main').width();
var heightz1 = $('#main').height();
$('#main')
    .css('width', widthz1)
    .css('height', heightz1);
var myChart2 = echarts.init(document.getElementById('main'));
var option = {
    title: {
        text: '今日街道处置率排行',
        top: '20',
        left: '5',
        textStyle: {
            color: '#0deae0',
            fontSize: 16,
            fontWeight: 'normal',

        },

    },
    grid: {
        left: '3%',
        top: '25%',
        bottom: '10%',
        containLabel: true
    },
    xAxis: {
        type: 'value',
        show: false,
        max: 100,
        min: 0,
        splitNumber: 2,
    },
    yAxis: {
        type: 'category',
        axisTick: {
            show: false
        },
        axisLabel: {
            show: true,
            interval: 0,
            textStyle: {
                fontSize: 12,
                color: '#fff',
            }
        },
        data: ['05      半山', '04      上塘', '03      湖墅', '02      小河', '01   拱宸桥',]
    },
    series: [{
        data: [75, 81, 87, 91, 95],
        type: 'bar',
        // 显示文字
        label: {
            normal: {
                show: true,
                position: 'right',
                formatter: '{c}%'
            },

        },
        itemStyle: {
            normal: {
                //这里是重点
                color: function (params) {
                    //注意，如果颜色太少的话，后面颜色不会自动循环，最好多定义几个颜色
                    var colorList = ['#8b53e0', '#f78c2f', '#c95c5c', '#298b4e', '#489df7'];
                    return colorList[params.dataIndex]
                }
            }
        },
        // 柱子的宽度
        barWidth: 12,
        barGap: '40%',
    }]
};
myChart2.setOption(option);
//屏幕自适应
window.addEventListener("resize", function () {
    myChart2.resize();
})

// 柱状图2
var widthz2 = $('#box').width();
var heightz2 = $('#box').height();
$('#box')
    .css('width', widthz2)
    .css('height', heightz2);
var myCharts = echarts.init(document.getElementById('box'));
var charts = {
    xAxis: {
        type: 'value',
        show: false,
        max: 120,
        min: 0,
        splitNumber: 2,
        interval: 0,
    },
    yAxis: {
        type: 'category',
        axisTick: {
            show: false
        },
        data: ['05    小学', '04    小学', '03    小学', '02    小学', '01    小学',],
        axisLabel: {
            show: true,
            interval: 0,
            textStyle: {
                fontSize: 10,
                color: '#fff',
            }
        },
        // 坐标轴两边留白
        boundaryGap: [0.2, 0.2]
    },
    grid: {
        left: '1%',
        top: '30%',
        bottom: '1%',
        containLabel: true
    },
    series: [{
        data: [75, 81, 87, 91, 95],
        type: 'bar',
        label: {
            normal: {
                show: true,
                position: 'right',
                formatter: '{c}%'
            }
        },
        itemStyle: {
            normal: {
                //这里是重点
                color: function (params) {
                    //注意，如果颜色太少的话，后面颜色不会自动循环，最好多定义几个颜色
                    var colorList = ['#8b53e0', '#f78c2f', '#c95c5c', '#298b4e', '#489df7'];
                    return colorList[params.dataIndex]
                }
            }
        },
        barCategoryGap: '40%',/*多个并排柱子设置柱子之间的间距*/

    }]
};
myCharts.setOption(charts);
//屏幕自适应
window.addEventListener("resize",function(){
	myCharts.resize();
})

// 0    小学
var myChart1 = echarts.init(document.getElementById('Bbox'));
var widthz3 = $('#Bbox').width();
var heightz3 = $('#Bbox').height();
$('#Bbox')
    .css('width', widthz3)
    .css('height', heightz3);
var chart1 = {
    xAxis: {
        type: 'value',
        show: false,
        max: 100,
        min: 0,
        splitNumber: 2,

    },
    yAxis: {
        type: 'category',
        axisTick: {
            show: false
        },
        data: ['05    小学', '04    小学', '03    小学', '02    小学', '01    小学',],
        axisLabel: {
            show: true,
            interval: 0,
            textStyle: {
                fontSize: 10,
                color: '#fff',
            }
        },
    },
    grid: {
        left: '2%',
        top: '15%',
        bottom: '45%',
        containLabel: true
    },
    series: [{
        data: [75, 81, 87, 91, 95],
        type: 'bar',
        label: {
            normal: {
                show: true,
                position: 'right',
                formatter: '{c}%'
            }

        },
        itemStyle: {
            normal: {
                //这里是重点
                color: function (params) {
                    //注意，如果颜色太少的话，后面颜色不会自动循环，最好多定义几个颜色
                    var colorList = ['#8b53e0', '#f78c2f', '#c95c5c', '#298b4e', '#489df7'];
                    return colorList[params.dataIndex]
                }
            }
        },
        barCategoryGap: '40%',/*多个并排柱子设置柱子之间的间距*/

    }]
};
myChart1.setOption(chart1);
//屏幕自适应
window.addEventListener("resize", function () {
    myChart1.resize();
})

// 饼图

var huan1 = echarts.init(document.getElementById('huan1'));
var excel1 = {
    title: {
        subtext: '告警总数',
        text: 92,
        x: 'center',
        y: 'center',
        //text和subtext之间的距离
        itemGap: 0.5,
        textStyle: {
            color: '#ccc',
            fontSize: 12,
            fontWeight: 'normal'
            // lineHeight:18,
        },
        subtextStyle: {
            color: '#7f9793',
            fontSize: 6,
        }

    },
    grid: {
        left: '2%',
        top: '1%',
        bottom: '1%',
        containLabel: true
    },
    tooltip: {
        trigger: 'item',
        formatter: '{a} <br/>{b}: {c} ({d}%)'
    },
    series: [
        {
            name: '今日告警占比',
            type: 'pie',
            // 饼图圆度 内圈,外圈
            radius: ['70%', '80%'],
            // 高亮扇区的偏移距离
            hoverOffset: 3,
            label: {
                show: false,
                position: 'center',
                normal: {
                    position: 'inner',
                    show: false
                }
            },
            // 高亮区样式
            emphasis: {
                label: {
                    show: true,
                    fontSize: '24',
                }
            },
            labelLine: {
                show: false
            },
            itemStyle: {
                normal: {
                    color: function (params) {
                        //自定义颜色
                        var colorList = [
                            '#29a479', '#244dc0', '#f47153'
                        ];
                        return colorList[params.dataIndex]
                    }
                }
            },
            data: [
                {value: 81, name: '上塘'},
                {value: 95, name: '拱宸桥'},
                {value: 91, name: '小河'},
            ],
        }
    ]
};
huan1.setOption(excel1);
//屏幕自适应
window.addEventListener("resize", function () {
    huan1.resize();
})

var huan2 = echarts.init(document.getElementById('huan2'));
var excel2 = {
    title: {
        subtext: '告警总数',
        text: 489,
        x: 'center',
        y: 'center',
        // 圆弧位置
        center: ['50%', '50%'],
        // 主副标题之间的距离
        itemGap: 1,
        textStyle: {
            color: '#ccc',
            fontSize: 16,
            fontWeight: 'normal'
        },
        subtextStyle: {
            color: '#7f9793',
            fontSize: 10,
        }

    },
    grid: {
        left: '1%',
        bottom: '10%',
        right: '0',
        // height:"66%",
        containLabel: true
    },
    tooltip: {
        trigger: 'item',
        formatter: '{a} <br/>{b}: {c} ({d}%)'
    },
    series: [
        {
            name: '今日告警占比',
            type: 'pie',
            radius: ['65%', '80%'],
            avoidLabelOverlap: false,
            hoverOffset: 5,
            label: {
                show: false,
                position: 'center',
                normal: {
                    position: 'inner',
                    show: false
                }
            },
            emphasis: {
                label: {
                    show: true,
                    fontSize: '30',
                }
            },
            labelLine: {
                show: false
            },
            data: [
                {value: 81, name: '上塘'},
                {value: 95, name: '拱宸桥'},
                {value: 91, name: '小河'},
                {value: 87, name: '湖墅'},
                {value: 75, name: '半山'},
                {value: 71, name: '康桥'}
            ],
        }
    ]
};

huan2.setOption(excel2);
//屏幕自适应
window.addEventListener("resize", function () {
    huan2.resize();
})


//地图
function fy() {


var dataJson = [
    {name: '上城区', value: 100}, {name: '下城区', value: 450},
    {name: '西湖区', value: 345}, {name: '拱墅区', value: 535},
    {name: '江干区', value: 320}, {name: '滨江区', value: 234},
    {name: '萧山区', value: 189}, {name: '余杭区', value: 99},
    {name: '富阳区', value: 79}, {name: '临安区', value: 190},
    {name: '桐庐县', value: 390}, {name: '淳安县', value: 360},
    {name: '建德市', value: 269},
];
//获取杭州市的地图信息
window.onload = function GetGeoMap() {
    var geoJson = null;
    var url = "js/full.json";
    MapChart.showLoading();
    $.get(url, null, function (ret) {
        geoJson = ret;
        loadMap(geoJson);

    });
}
//初始化地图容器
var MapChart = echarts.init(document.getElementById('china_map'));
var widthmap = $('#china_map').width();
var heightmap = $('#china_map').height();
$('#china_map')
    .css('width', widthmap)
    .css('height', heightmap);
//设置地图。
var loadMap = function (geoJson) {
    echarts.registerMap('tianjin', geoJson);
    MapChart.hideLoading();
    var data = dataJson;
    // 地图中心点坐标
    var geoCoordMap = {
        '上城区': [120.171465, 30.215236],
        '下城区': [120.172763, 30.326271],
        '西湖区': [120.027376, 30.132934],
        '拱墅区': [120.204053, 30.374697],
        '江干区': [120.362633, 30.276603],
        '滨江区': [120.198623, 30.166615],
        '萧山区': [120.150693, 29.962932],
        '余杭区': [119.801737, 30.421187],
        '富阳区': [119.949869, 29.849871],
        '临安区': [119.315101, 30.231153],
        '桐庐县': [119.585045, 29.797437],
        '淳安县': [118.624346, 29.404177],
        '建德市': [119.279089, 29.472284],
    }
    var convertData = function (data) {
        var res = [];
        for (var i = 0; i < data.length; i++) {
            var geoCoord = geoCoordMap[data[i].name];
            if (geoCoord) {
                res.push({
                    name: data[i].name,
                    value: geoCoord.concat(data[i].value)
                });
            }
        }
        return res;
    };
    var option = {
        tooltip: {
            trigger: 'item',
            formatter: function (params) {
                if (typeof (params.value)[2] == "undefined") {
                    return params.name + ' : ' + params.value;
                } else {
                    return params.name + ' : ' + params.value[2];
                }
            }
        },
        // 导航
        visualMap: {
            show: false,
            type: 'piecewise',
            pieces: [
                {min: 600},
                {min: 400, max: 599},
                {min: 300, max: 399},
                {min: 200, max: 299},
                {min: 100, max: 199},
                {min: 0, max: 99},

            ],
            seriesIndex: [1],
            inRange: {
                color: ['#8b53e0', '#f78c2f', '#c95c5c', '#298b4e', '#489df7']
            }
        },
        geo: {
            show: true,
            map: 'tianjin',
            // 地图位置偏移 左，上
            layoutCenter: ['46%', '40   %'],
            // 地图总大小
            layoutSize: '100%',
            label: {
                normal: {
                    show: false
                },
                emphasis: {
                    show: false,
                }
            },
            itemStyle: {
                normal: {
                    areaColor: '#031525',
                    borderColor: '#fff',
                    borderWidth: 2,
                },
                emphasis: {
                    areaColor: '#56ddff', //鼠标放上去的亮色
                }
            }
        },
        series: [
            {
                name: 'credit_pm2.5',
                type: 'scatter',
                coordinateSystem: 'geo',
                data: convertData(data),
                // 标记大小
                symbolSize: function (val) {
                    return 5;
                },
                label: {
                    normal: {
                        formatter: '{b}',
                        position: 'right',
                        show: true
                    },
                    emphasis: {
                        show: true
                    }
                },
                itemStyle: {
                    normal: {
                        color: '#fff',
                        areaColor: '#FBE805',
                        // borderColor: '#FBE805',
                    },
                    emphasis: {
                        areaColor: '#FBE805'
                    }
                },

            },
            {
                type: 'map',
                map: 'tianjin',
                geoIndex: 0,
                aspectScale: 0.5, //长宽比
                showLegendSymbol: false, // 存在legend时显示
                label: {
                    normal: {
                        show: false
                    },
                    emphasis: {
                        show: false,
                        textStyle: {
                            color: '#fff'
                        }
                    }
                },
                // roam: true,
                itemStyle: {
                    normal: {
                        areaColor: '#031525',
                        borderColor: '#3B5077',
                    },
                    emphasis: {
                        areaColor: '#2B91B7'
                    }
                },
                animation: false,
                data: data
            },
            {
                name: '点',
                type: 'scatter',
                coordinateSystem: 'geo',
                symbol: 'pin',
                data: convertData(data),
                //气泡大小
                symbolSize: function (val) {

                    return 40;
                },
                label: {
                    normal: {
                        show: true,
                        formatter: function (params) {
                            if (typeof (params.value)[2] == "undefined") {
                                return params.value;
                            } else {
                                return params.value[2];
                            }
                        },//将集合中序号为2的显示在气泡上，默认为1{x,y,val
                        textStyle: {
                            color: '#fff',//文字颜色
                            fontSize: 9,
                        }
                    }
                },
                itemStyle: {
                    normal: {
                        color: '#2080F7', //标志颜色
                    }
                },
                zlevel: 6,

            },
            {
                name: 'Top 5',
                type: 'effectScatter',
                coordinateSystem: 'geo',
                data: convertData(data.sort(function (a, b) {
                    return b.value - a.value;
                }).slice(0, 1)),
                // 标记大小
                symbolSize: function (val) {
                    return 5;
                },
                showEffectOn: 'render',
                rippleEffect: {
                    brushType: 'stroke'
                },
                hoverAnimation: true,
                label: {
                    normal: {
                        formatter: '{b}',
                        position: 'right',
                        show: true
                    }
                },
                itemStyle: {
                    normal: {
                        color: '#fff',
                        shadowBlur: 10,
                        shadowColor: '#dece00'
                    }
                },
                zlevel: 1
            },

        ]
    };

    MapChart.setOption(option);
// GetUserCountByAre("");
}
//屏幕自适应
window.addEventListener("resize", function () {
    MapChart.resize();
})
}
fy();