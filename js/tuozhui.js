//折线图
function chartresize(){
    let arr = Array.from(document.querySelectorAll('.c1'));
    let charArr = arr.map(item=>echarts.init(item))
    // 配置数据
    var option1 = {
        width: '100%',
        height: '90%',
        xAxis: {
            type: 'category',
            data: ['8.1', '8.2', '8.3', '8.4', '8.5', '8.6', '8.7'],
            axisLine: {
                lineStyle: {
                    color: '#eee',
                    width: 1, //这里是为了突出显示加上的
                }
            },
            //X轴刻度平均分布
            axisLabel: {
                fontSize: '20rem',
                interval: 0
            }
        },
        yAxis: {
            type: 'value',
            axisTick: {
                show: false
            },
            //刻度线间距
            max: 300,
            min: 0,
            splitNumber: 3,
            axisLabel: {
                fontSize: '20rem'
            },
            //XY轴样式
            axisLine: {
                show: false,
                lineStyle: {
                    color: '#ccc',
                    width: 1, //这里是为了突出显示加上的

                },

            },
            //网格线
            splitLine: {
                show: true,
                lineStyle: {
                    color: '#0ea578',
                    width: 0.5,
                    type: 'solid',
                }
            },

        },
        //偏移值
        grid: {
            left: '1%',
            top: '10%',
            bottom: '10%',
            right: '1%',
            width: 'auto',
            height: 'auto',
            containLabel: true
        },
        series: [
            {
                type: 'line',
                //曲线弯度
                smooth: 0.7,
                areaStyle: {
                    normal: {}
                },
                data: [129, 210, 98, 229, 218, 96, 184],
                itemStyle: {
                    normal: {
                        //折线区域颜色渐变
                        color: new echarts.graphic.LinearGradient(0, 0, 0, 1[
                            {
                                offset: 0,
                                color: '#1C494B'
                            },
                                {
                                    offset: 0.5,
                                    color: 'rgba(255,255,255,0.5)'
                                },
                                {
                                    offset: 1,
                                    color: 'transparent'
                                }
                            ]),
                        color: '#074F4B', //改变折线点的颜色
                        borderColor: '#075652',
                        lineStyle: {
                            color: '#0ea377', //改变折线颜色
                            width: 1
                        }
                    }
                },
                label: {
                    normal: {
                        show: true,
                        position: 'top',
                        color: '#fff',
                        fontSize: 9,
                    }
                }
            }
        ]
    };
    // 渲染图表
    charArr.forEach(item=>item.setOption(option1))
}

function chartresize1(){
    let arr = Array.from(document.querySelectorAll(".myMain"));
    let charArr = arr.map(item=>echarts.init(item))
    var option2 = {
        // 图形宽高
        width: '100%',
        height: '100%',
        // 标题
        title: {
            text: '今日街道处置率排行',
            top: '5',
            left: '5',
            textStyle: {
                color: '#0deae0',
                fontSize: 16,
                fontWeight: 'normal',

            },

        },
        // 偏移值
        grid: {
            left: '1%',
            top: '20%',
            bottom: '1%',
            right: '5%',
            width: 'auto',
            height: 'auto',
            containLabel: true
        },
        xAxis: {
            type: 'value',
            show: false,
            max: 100,
            min: 0,
            splitNumber: 2,
        },
        yAxis: {
            type: 'category',
            axisTick: {
                show: false
            },
            axisLabel: {
                show: true,
                interval: 0,
                textStyle: {
                    fontSize: 12,
                    color: '#fff',
                }
            },
            data: ['05      半山', '04      上塘', '03      湖墅', '02      小河', '01   拱宸桥',]
        },
        series: [{
            data: [75, 81, 87, 91, 95],
            type: 'bar',
            // 柱子后面显示的文字
            label: {
                normal: {
                    show: true,
                    position: 'right',
                    formatter: '{c}%'
                },

            },
            // 自定义柱子颜色
            itemStyle: {
                normal: {
                    //这里是重点
                    color: function (params) {
                        //注意，如果颜色太少的话，后面颜色不会自动循环，最好多定义几个颜色
                        var colorList = ['#8b53e0', '#f78c2f', '#c95c5c', '#298b4e', '#489df7'];
                        return colorList[params.dataIndex]
                    }
                }
            },
            // 柱子宽度
            barWidth: 12,
            barGap: '40%',
        }]
    };
    charArr.forEach(item=>item.setOption(option2))
}

function chartresize2() {

    let arr = Array.from(document.querySelectorAll(".huan"));
    let charArr = arr.map(item=>echarts.init(item))
    var option3 = {
        title: {
            subtext: '告警总数',
            text: 489,
            x: 'center',
            y: 'center',
            // 圆弧位置
            center: ['50%', '50%'],
            // 主副标题之间的距离
            itemGap: 1,
            textStyle: {
                color: '#ccc',
                fontSize: 16,
                fontWeight: 'normal'
            },
            subtextStyle: {
                color: '#7f9793',
                fontSize: 10,
            }

        },
        grid: {
            left: '1%',
            bottom: '10%',
            right: '0',
            containLabel: true
        },
        tooltip: {
            trigger: 'item',
            formatter: '{a} <br/>{b}: {c} ({d}%)'
        },
        series: [
            {
                name: '今日告警占比',
                type: 'pie',
                radius: ['65%', '80%'],
                avoidLabelOverlap: false,
                hoverOffset: 5,
                label: {
                    show: true,

                },
                emphasis: {
                    label: {
                        show: true,
                        fontSize: '30',
                    }
                },
                labelLine: {
                    show: true
                },
                data: [
                    {value: 81, name: '上塘'},
                    {value: 95, name: '拱宸桥'},
                    {value: 91, name: '小河'},
                    {value: 87, name: '湖墅'},
                    {value: 75, name: '半山'},
                    {value: 71, name: '康桥'}
                ],
            }
        ]
    };
    charArr.forEach(item=>item.setOption(option3))
}

function chartresize3(){
    var dataJson = [
        {name: '上城区', value: 100}, {name: '下城区', value: 450},
        {name: '西湖区', value: 345}, {name: '拱墅区', value: 535},
        {name: '江干区', value: 320}, {name: '滨江区', value: 234},
        {name: '萧山区', value: 189}, {name: '余杭区', value: 99},
        {name: '富阳区', value: 79}, {name: '临安区', value: 190},
        {name: '桐庐县', value: 390}, {name: '淳安县', value: 360},
        {name: '建德市', value: 269},
    ];
    //获取杭州市的地图信息
    var geoJson = null;
    var url = "js/full.json";
    $.get(url, null, function (ret) {
        geoJson = ret;
        loadMap(geoJson);

    });
    var arr = Array.from(document.querySelectorAll(".map"));
    var charArr = arr.map(item=>echarts.init(item))
    var loadMap = function (geoJson) {
        echarts.registerMap('tianjin', geoJson);
        var data = dataJson;
        var geoCoordMap = {
            '上城区': [120.171465, 30.215236],
            '下城区': [120.172763, 30.326271],
            '西湖区': [120.027376, 30.132934],
            '拱墅区': [120.204053, 30.374697],
            '江干区': [120.362633, 30.276603],
            '滨江区': [120.198623, 30.166615],
            '萧山区': [120.150693, 29.962932],
            '余杭区': [119.801737, 30.421187],
            '富阳区': [119.949869, 29.849871],
            '临安区': [119.315101, 30.231153],
            '桐庐县': [119.585045, 29.797437],
            '淳安县': [118.624346, 29.404177],
            '建德市': [119.279089, 29.472284],
        }
        var convertData = function (data) {
            var res = [];
            for (var i = 0; i < data.length; i++) {
                var geoCoord = geoCoordMap[data[i].name];
                if (geoCoord) {
                    res.push({
                        name: data[i].name,
                        value: geoCoord.concat(data[i].value)
                    });
                }
            }
            return res;
        };
        // 配置数据
        var option = {
            tooltip: {
                trigger: 'item',
                formatter: function (params) {
                    if (typeof (params.value)[2] == "undefined") {
                        return params.name + ' : ' + params.value;
                    } else {
                        return params.name + ' : ' + params.value[2];
                    }
                }
            },
            // 导航
            visualMap: {
                show: false,
                type: 'piecewise',
                pieces: [
                    {min: 600},
                    {min: 400, max: 599},
                    {min: 300, max: 399},
                    {min: 200, max: 299},
                    {min: 100, max: 199},
                    {min: 0, max: 99},

                ],
                seriesIndex: [1],
                inRange: {
                    color: ['#8b53e0', '#f78c2f', '#c95c5c', '#298b4e', '#489df7']
                }
            },
            geo: {
                show: true,
                map: 'tianjin',
                // 地图位置偏移 左，上
                layoutCenter: ['50%', '50%'],
                // 地图总大小
                layoutSize: '100%',
                label: {
                    normal: {
                        show: false
                    },
                    emphasis: {
                        show: false,
                    }
                },
                itemStyle: {
                    normal: {
                        areaColor: '#031525',
                        borderColor: '#fff',
                        borderWidth: 2,
                    },
                    emphasis: {
                        areaColor: '#56ddff', //鼠标放上去的亮色
                    }
                }
            },
            series: [
                {
                    name: 'credit_pm2.5',
                    type: 'scatter',
                    coordinateSystem: 'geo',
                    data: convertData(data),
                    // 标记大小
                    symbolSize: function (val) {
                        return 5;
                    },
                    label: {
                        normal: {
                            formatter: '{b}',
                            position: 'right',
                            show: true
                        },
                        emphasis: {
                            show: true
                        }
                    },
                    itemStyle: {
                        normal: {
                            color: '#fff',
                            areaColor: '#FBE805',
                            // borderColor: '#FBE805',
                        },
                        emphasis: {
                            areaColor: '#FBE805'
                        }
                    },

                },
                {
                    type: 'map',
                    map: 'tianjin',
                    geoIndex: 0,
                    aspectScale: 0.5, //长宽比
                    showLegendSymbol: false, // 存在legend时显示
                    label: {
                        normal: {
                            show: false
                        },
                        emphasis: {
                            show: false,
                            textStyle: {
                                color: '#fff'
                            }
                        }
                    },
                    // roam: true,
                    itemStyle: {
                        normal: {
                            areaColor: '#031525',
                            borderColor: '#3B5077',
                        },
                        emphasis: {
                            areaColor: '#2B91B7'
                        }
                    },
                    animation: false,
                    data: data
                },
                {
                    name: '点',
                    type: 'scatter',
                    coordinateSystem: 'geo',
                    symbol: 'pin',
                    data: convertData(data),
                    //气泡大小
                    symbolSize: function (val) {

                        return 40;
                    },
                    label: {
                        normal: {
                            show: true,
                            formatter: function (params) {
                                if (typeof (params.value)[2] == "undefined") {
                                    return params.value;
                                } else {
                                    return params.value[2];
                                }
                            },//将集合中序号为2的显示在气泡上，默认为1{x,y,val
                            textStyle: {
                                color: '#fff',//文字颜色
                                fontSize: 9,
                            }
                        }
                    },
                    itemStyle: {
                        normal: {
                            color: '#2080F7', //标志颜色
                        }
                    },
                    zlevel: 6,

                },
                {
                    name: 'Top 5',
                    type: 'effectScatter',
                    coordinateSystem: 'geo',
                    data: convertData(data.sort(function (a, b) {
                        return b.value - a.value;
                    }).slice(0, 1)),
                    // 标记大小
                    symbolSize: function (val) {
                        return 5;
                    },
                    showEffectOn: 'render',
                    rippleEffect: {
                        brushType: 'stroke'
                    },
                    hoverAnimation: true,
                    label: {
                        normal: {
                            formatter: '{b}',
                            position: 'right',
                            show: true
                        }
                    },
                    itemStyle: {
                        normal: {
                            color: '#fff',
                            shadowBlur: 10,
                            shadowColor: '#dece00'
                        }
                    },
                    zlevel: 1
                },

            ]
        };
        // 渲染图表
        charArr.forEach(item=>item.setOption(option))
    }
}



$(function () {
    var i =0;
    $('.left1')
        .click(function () {
            var div = $(`<div class='dragable tu${i}'></div>`);
            $('#container').append(div);
            div.append(`<div class="XY"></div>
                <div class="cont">
                    <div class="div">
                        <div class="c1" style="width:100%;height:100%;" id="chart${i}"></div>
                    </div>
                    <img src="./images/del.png" class="img">
                </div>`)
            // echarts重新渲染
            chartresize();
            var c1 = echarts.init(document.getElementById('chart'+i+''));
            window.addEventListener("click", function () {
                c1.resize();
            })
            //拖拽  缩放
            tuozhuai();
            // 删除
            del();
            // 定位
            posi();

            i++;
            // $('.dragable').children('.XY').text('');
        })
    $('.left2')
        .click(function () {
            var t2 = $('.t2').html();
            var div = $("<div></div>");
            div.addClass('dragable')
            $('#container').append(div);
            div.append(t2);
            //拖拽
            tuozhuai();
            // 删除
            del();
            // 定位
            posi();
        })
    $('.left3')
        .click(function () {
            var t3 = $(".t3").html();
            var div = $("<div></div>");
            div.addClass('dragable')
            $('#container').append(div);
            div.append(t3);
            //拖拽
            tuozhuai();
            // 删除
            del();
            // 定位
            posi();
        })
    $('.left4')
        .click(function () {
            var div = $(`<div class='dragable tu${i}'></div>`);
            $('#container').append(div);
            div.append(`<div class="XY"></div>
                <div class="cont">
                    <div class="div">
                        <div class="myMain" style="width:100%;height:100%;" id="myMain${i}"></div>
                    </div>
                    <img src="./images/del.png" class="img">
                </div>`)
            chartresize1();
            var c2 = echarts.init(document.getElementById('myMain'+i+''));
            window.addEventListener("click", function () {
                c2.resize();
            })
            //拖拽
            tuozhuai();
            // 删除
            del();
            // 定位
            posi();
            i++;
        })
    $('.left5')
        .click(function () {
            var div = $(`<div class='dragable tu${i}'></div>`);
            $('#container').append(div);
            div.append(`<div class="XY"></div>
                <div class="cont">
                    <div class="div">
                        <div class="huan" style="width:100%;height:100%;" id="huan${i}"></div>
                    </div>
                    <img src="./images/del.png" class="img">
                </div>`)
            chartresize2();
            var c3 = echarts.init(document.getElementById('huan'+i+''));
            window.addEventListener("click", function () {
                c3.resize();
            })
            //拖拽
            tuozhuai();
            // 删除
            del();
            // 定位
            posi();
            i++;
        })
    $('.left6')
        .click(function () {
            var div = $(`<div class='dragable tu${i}'></div>`);
            $('#container').append(div);
            div.append(`<div class="XY"></div>
                <div class="cont">
                    <div class="div">
                        <div class="map" style="width:100%;height:100%;" id="map${i}"></div>
                    </div>
                    <img src="./images/del.png" class="img">
                </div>`)
            chartresize3();
            var map = echarts.init(document.getElementById('map'+i+''));
            window.addEventListener("click", function () {
                map.resize();
            })
            //拖拽
            tuozhuai();
            // 删除
            del();
            // 定位
            posi();
            i++;
        })
});

var n = 0;
// 获取初始定位
function posi(){

    console.log('i:'+n);
    var X1 = $('.tu'+n+'').offset().left;
    var Y1 = $('.tu'+n+'').offset().top;
    console.log('X1:'+X1);
    console.log('Y1:'+Y1);
    // var X2 = $('.dragable').parents('body').position().left;
    // var Y2 = $('.dragable').parents('body').position().top;
    $('.tu'+n+'').children('.XY').text(parseInt(X1) + ',' + parseInt(Y1));
    $('.tu'+n+'').css({left:X1,top:Y1});
    ++n;
    console.log('i+:'+n);
}

// 拖拽 缩放
function tuozhuai() {
    $('.dragable')
        .resizable({
            maxWidth: 1200,
            maxHeight: 840,
            //缩放方向
            handles: "all"
        })
        .draggable({
            //可拖动区域
            containment: '#container',
            drag: function () {
                var $this = $(this);
                var thisPos = $this.position();
                // var parentPos = $this.parent().parent().parent().parent().position();
                var x = parseInt(thisPos.left);
                var y = parseInt(thisPos.top);
                $this.children(".XY").text(x + "," + y);
            }
        })

}

// 元素删除
function del() {
    $('.img').on("click", function () {
        $(this).parent().parent().remove();
    })
}


